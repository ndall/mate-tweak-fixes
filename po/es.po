# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Emiliano Fascetti, 2015
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-17 11:19+0000\n"
"PO-Revision-Date: 2015-08-13 11:24+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Spanish (http://www.transifex.com/mate/MATE/language/es/)\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../mate-tweak:831
msgid "Marco (No compositor)"
msgstr ""

#: ../mate-tweak:833
msgid "Marco (Software compositor)"
msgstr ""

#: ../mate-tweak:835
msgid "Marco (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:837
msgid "Metacity (No compositor)"
msgstr ""

#: ../mate-tweak:839
msgid "Metacity (Software compositor)"
msgstr ""

#: ../mate-tweak:841
msgid "Metacity (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:843
msgid "Mutter (Elegant GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:845
msgid "Compiz (Advanced GPU accelerated desktop effects)"
msgstr "Compiz (efectos avanzados de escritorio acelerados por GPU)"

#: ../mate-tweak:862
msgid "Cupertino"
msgstr ""

#: ../mate-tweak:865
msgid "Fedora"
msgstr ""

#: ../mate-tweak:868
msgid "GNOME2"
msgstr ""

#: ../mate-tweak:871
msgid "Linux Mint"
msgstr "Linux Mint"

#: ../mate-tweak:874
msgid "Mageia"
msgstr ""

#: ../mate-tweak:879
msgid "Mutiny"
msgstr ""

#: ../mate-tweak:882
msgid "Netbook"
msgstr ""

#: ../mate-tweak:885
msgid "openSUSE"
msgstr "openSUSE"

#: ../mate-tweak:888
msgid "Redmond"
msgstr "Redmond"

#: ../mate-tweak:891
msgid "Ubuntu MATE"
msgstr "Ubuntu MATE"

#: ../mate-tweak:894
msgid "Wimpy"
msgstr ""

#: ../mate-tweak:944
msgid "Desktop"
msgstr "Escritorio"

#: ../mate-tweak:945
msgid "Windows"
msgstr "Ventanas"

#: ../mate-tweak:946
msgid "Interface"
msgstr "Interfaz"

#: ../mate-tweak:989
msgid "MATE Tweak"
msgstr ""

#: ../mate-tweak:992
msgid "Desktop icons"
msgstr "Iconos de escritorio"

#: ../mate-tweak:993
msgid "Performance"
msgstr "Rendimiento"

#: ../mate-tweak:994
msgid "Window Behaviour"
msgstr ""

#: ../mate-tweak:995
msgid "Appearance"
msgstr "Apariencia"

#: ../mate-tweak:996
msgid "Panels"
msgstr ""

#: ../mate-tweak:997
msgid "Panel Features"
msgstr ""

#: ../mate-tweak:998
msgid "Icons"
msgstr "Iconos"

#: ../mate-tweak:999
msgid "Context menus"
msgstr "Menús contextuales"

#: ../mate-tweak:1000
msgid "Toolbars"
msgstr "Barras de herramientas"

#: ../mate-tweak:1001
msgid "Window manager"
msgstr ""

#: ../mate-tweak:1003
#, fuzzy
msgid "Select the Desktop Icons you want enabled:"
msgstr "Seleccione los elementos que quiere ver en el escritorio:"

#: ../mate-tweak:1004
#, fuzzy
msgid "Show Desktop Icons"
msgstr "Iconos de escritorio"

#: ../mate-tweak:1005
msgid "Computer"
msgstr "Equipo"

#: ../mate-tweak:1006
msgid "Home"
msgstr "Carpeta personal"

#: ../mate-tweak:1007
msgid "Network"
msgstr "Red"

#: ../mate-tweak:1008
msgid "Trash"
msgstr "Papelera"

#: ../mate-tweak:1009
msgid "Mounted Volumes"
msgstr "Volúmenes montados"

#: ../mate-tweak:1011
msgid "Enable animations"
msgstr ""

#: ../mate-tweak:1012
msgid "Do not show window content when moving windows"
msgstr ""

#: ../mate-tweak:1013
msgid "Window manager performance tuning."
msgstr ""

#: ../mate-tweak:1015
msgid "Enable window snapping"
msgstr ""

#: ../mate-tweak:1016
msgid "Undecorate maximized windows"
msgstr ""

#: ../mate-tweak:1017
msgid "Do not auto-maximize new windows"
msgstr ""

#: ../mate-tweak:1019
msgid "Window control placement."
msgstr ""

#: ../mate-tweak:1021
msgid "Save Panel Layout"
msgstr ""

#: ../mate-tweak:1022
msgid "Enable indicators"
msgstr ""

#: ../mate-tweak:1023
msgid "Enable advanced menu"
msgstr ""

#: ../mate-tweak:1024
msgid "Enable keyboard LED"
msgstr ""

#: ../mate-tweak:1025
msgid "Enable launcher"
msgstr ""

#: ../mate-tweak:1026
msgid "Show icons on menus"
msgstr "Mostrar iconos en los menús"

#: ../mate-tweak:1027
msgid "Show icons on buttons"
msgstr "Mostrar iconos en los botones"

#: ../mate-tweak:1028
msgid "Show Input Methods menu in context menus"
msgstr "Mostrar menú de métodos de entrada en menús contextuales"

#: ../mate-tweak:1029
msgid "Show Unicode Control Character menu in context menus"
msgstr "Mostrar menú de caracteres de control Unicode  en menús contextuales"

#: ../mate-tweak:1031
msgid "Style:"
msgstr ""

#: ../mate-tweak:1032
msgid "Icon size:"
msgstr "Tamaño de los iconos:"

#: ../mate-tweak:1051
msgid "Small"
msgstr "Pequeño"

#: ../mate-tweak:1052
msgid "Large"
msgstr "Grande"

#: ../mate-tweak:1058
msgid "Traditional (Right)"
msgstr "Tradicional (derecha)"

#: ../mate-tweak:1059
msgid "Contemporary (Left)"
msgstr "Contemporáneo (izquierda)"

#: ../mate-tweak:1065
msgid "The new window manager will be activated upon selection."
msgstr "El nuevo gestor de ventanas se activará al seleccionar."

#: ../mate-tweak:1066
msgid "Select a window manager."
msgstr ""

#: ../mate-tweak:1072
msgid "Select a panel layout to change the user interface."
msgstr ""

#: ../mate-tweak:1073
msgid ""
"The new panel layout will be activated on selection and destroy any "
"customisations you might have made."
msgstr ""
"El nuevo diseño del panel se activará después de la selección y destruirá "
"cualquier personalización que haya realizado."

#: ../mate-tweak:1086
msgid "Text below items"
msgstr "Texto debajo de los elementos"

#: ../mate-tweak:1087
msgid "Text beside items"
msgstr "Texto al lado de los elementos"

#: ../mate-tweak:1088
msgid "Icons only"
msgstr "Sólo iconos"

#: ../mate-tweak:1089
msgid "Text only"
msgstr "Sólo texto"

#~ msgid "Use compositing"
#~ msgstr "Usar Composición"

#~ msgid "Marco (Simple desktop effects)"
#~ msgstr "Marco (efectos de escritorio simples)"

#, fuzzy
#~ msgid "Marco (Simple GPU accelerated desktop effects)"
#~ msgstr "Marco (efectos de escritorio simples)"

#, fuzzy
#~ msgid "Metacity (Simple GPU accelerated desktop effects)"
#~ msgstr "Compiz (efectos avanzados de escritorio acelerados por GPU)"

#, fuzzy
#~ msgid "Muffin (Elegant GPU accelerated desktop effects)"
#~ msgstr "Compiz (efectos avanzados de escritorio acelerados por GPU)"
