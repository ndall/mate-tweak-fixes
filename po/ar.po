# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-17 11:19+0000\n"
"PO-Revision-Date: 2015-08-13 11:24+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Arabic (http://www.transifex.com/mate/MATE/language/ar/)\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: ../mate-tweak:831
msgid "Marco (No compositor)"
msgstr ""

#: ../mate-tweak:833
msgid "Marco (Software compositor)"
msgstr ""

#: ../mate-tweak:835
msgid "Marco (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:837
msgid "Metacity (No compositor)"
msgstr ""

#: ../mate-tweak:839
msgid "Metacity (Software compositor)"
msgstr ""

#: ../mate-tweak:841
msgid "Metacity (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:843
msgid "Mutter (Elegant GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:845
msgid "Compiz (Advanced GPU accelerated desktop effects)"
msgstr "كومبيز ( تأثيرات رسومية مسرّعة متقدمة لسطح المكتب)"

#: ../mate-tweak:862
msgid "Cupertino"
msgstr ""

#: ../mate-tweak:865
msgid "Fedora"
msgstr ""

#: ../mate-tweak:868
msgid "GNOME2"
msgstr ""

#: ../mate-tweak:871
msgid "Linux Mint"
msgstr "لينكس منت"

#: ../mate-tweak:874
msgid "Mageia"
msgstr ""

#: ../mate-tweak:879
msgid "Mutiny"
msgstr ""

#: ../mate-tweak:882
msgid "Netbook"
msgstr ""

#: ../mate-tweak:885
msgid "openSUSE"
msgstr "أوبن سوزي"

#: ../mate-tweak:888
msgid "Redmond"
msgstr "ريدموند"

#: ../mate-tweak:891
msgid "Ubuntu MATE"
msgstr "أوبنتو متّة"

#: ../mate-tweak:894
msgid "Wimpy"
msgstr ""

#: ../mate-tweak:944
msgid "Desktop"
msgstr "سطح المكتب"

#: ../mate-tweak:945
msgid "Windows"
msgstr "النوافذ"

#: ../mate-tweak:946
msgid "Interface"
msgstr "الواجهة"

#: ../mate-tweak:989
msgid "MATE Tweak"
msgstr ""

#: ../mate-tweak:992
msgid "Desktop icons"
msgstr "أيقونات سطح المكتب"

#: ../mate-tweak:993
msgid "Performance"
msgstr "الأداء"

#: ../mate-tweak:994
msgid "Window Behaviour"
msgstr ""

#: ../mate-tweak:995
msgid "Appearance"
msgstr "المظهر"

#: ../mate-tweak:996
msgid "Panels"
msgstr ""

#: ../mate-tweak:997
msgid "Panel Features"
msgstr ""

#: ../mate-tweak:998
msgid "Icons"
msgstr "الأيقونات"

#: ../mate-tweak:999
msgid "Context menus"
msgstr "القوائم الجانبية"

#: ../mate-tweak:1000
msgid "Toolbars"
msgstr "أشرطة الأدوات"

#: ../mate-tweak:1001
msgid "Window manager"
msgstr ""

#: ../mate-tweak:1003
#, fuzzy
msgid "Select the Desktop Icons you want enabled:"
msgstr "اختر العناصر التي تريد ظهورها على سطح المكتب:"

#: ../mate-tweak:1004
#, fuzzy
msgid "Show Desktop Icons"
msgstr "أيقونات سطح المكتب"

#: ../mate-tweak:1005
msgid "Computer"
msgstr "الحاسوب"

#: ../mate-tweak:1006
msgid "Home"
msgstr "المنزل"

#: ../mate-tweak:1007
msgid "Network"
msgstr "الشّبكة"

#: ../mate-tweak:1008
msgid "Trash"
msgstr "سلة المهملات"

#: ../mate-tweak:1009
msgid "Mounted Volumes"
msgstr "الأجزاء الموصولة"

#: ../mate-tweak:1011
msgid "Enable animations"
msgstr ""

#: ../mate-tweak:1012
msgid "Do not show window content when moving windows"
msgstr ""

#: ../mate-tweak:1013
msgid "Window manager performance tuning."
msgstr ""

#: ../mate-tweak:1015
msgid "Enable window snapping"
msgstr ""

#: ../mate-tweak:1016
msgid "Undecorate maximized windows"
msgstr ""

#: ../mate-tweak:1017
msgid "Do not auto-maximize new windows"
msgstr ""

#: ../mate-tweak:1019
msgid "Window control placement."
msgstr ""

#: ../mate-tweak:1021
msgid "Save Panel Layout"
msgstr ""

#: ../mate-tweak:1022
msgid "Enable indicators"
msgstr ""

#: ../mate-tweak:1023
msgid "Enable advanced menu"
msgstr ""

#: ../mate-tweak:1024
msgid "Enable keyboard LED"
msgstr ""

#: ../mate-tweak:1025
msgid "Enable launcher"
msgstr ""

#: ../mate-tweak:1026
msgid "Show icons on menus"
msgstr "اظهر الأيقونات على القوائم"

#: ../mate-tweak:1027
msgid "Show icons on buttons"
msgstr "أظهر الأيقونات في الأزرار"

#: ../mate-tweak:1028
msgid "Show Input Methods menu in context menus"
msgstr "اظهر قائمة طرق الادخال في القوائم الجانبية"

#: ../mate-tweak:1029
msgid "Show Unicode Control Character menu in context menus"
msgstr "اظهر قائمة التحكم بحروف اليونيكود في القوائم الجانبية"

#: ../mate-tweak:1031
msgid "Style:"
msgstr ""

#: ../mate-tweak:1032
msgid "Icon size:"
msgstr "حجم الأيقونة:"

#: ../mate-tweak:1051
msgid "Small"
msgstr "صغير"

#: ../mate-tweak:1052
msgid "Large"
msgstr "عريض"

#: ../mate-tweak:1058
msgid "Traditional (Right)"
msgstr "تقليدي (يمين)"

#: ../mate-tweak:1059
msgid "Contemporary (Left)"
msgstr "مُعاصر (يسار)"

#: ../mate-tweak:1065
msgid "The new window manager will be activated upon selection."
msgstr "سيُنشّط مديرُ النوافذ الجديدُ عند التحديد."

#: ../mate-tweak:1066
msgid "Select a window manager."
msgstr ""

#: ../mate-tweak:1072
msgid "Select a panel layout to change the user interface."
msgstr ""

#: ../mate-tweak:1073
msgid ""
"The new panel layout will be activated on selection and destroy any "
"customisations you might have made."
msgstr "سيُنشّط تصميمُ الشريط الجديدُ عند التحديد و سيحذف أي تخصيص قد قمت به."

#: ../mate-tweak:1086
msgid "Text below items"
msgstr "النص تحت العناصر"

#: ../mate-tweak:1087
msgid "Text beside items"
msgstr "النص بجانب العناصر"

#: ../mate-tweak:1088
msgid "Icons only"
msgstr "أيقونات فقط"

#: ../mate-tweak:1089
msgid "Text only"
msgstr "نص فقط"

#~ msgid "Use compositing"
#~ msgstr "استخدام التأليف"

#~ msgid "Marco (Simple desktop effects)"
#~ msgstr "ماركو ( تأثيرات بسيطة لسطح المكتب)"

#, fuzzy
#~ msgid "Marco (Simple GPU accelerated desktop effects)"
#~ msgstr "ماركو ( تأثيرات بسيطة لسطح المكتب)"

#, fuzzy
#~ msgid "Metacity (Simple GPU accelerated desktop effects)"
#~ msgstr "كومبيز ( تأثيرات رسومية مسرّعة متقدمة لسطح المكتب)"

#, fuzzy
#~ msgid "Muffin (Elegant GPU accelerated desktop effects)"
#~ msgstr "كومبيز ( تأثيرات رسومية مسرّعة متقدمة لسطح المكتب)"
