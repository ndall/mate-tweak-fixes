# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-17 11:19+0000\n"
"PO-Revision-Date: 2015-08-13 11:24+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Luxembourgish (http://www.transifex.com/mate/MATE/language/"
"lb/)\n"
"Language: lb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../mate-tweak:831
msgid "Marco (No compositor)"
msgstr ""

#: ../mate-tweak:833
msgid "Marco (Software compositor)"
msgstr ""

#: ../mate-tweak:835
msgid "Marco (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:837
msgid "Metacity (No compositor)"
msgstr ""

#: ../mate-tweak:839
msgid "Metacity (Software compositor)"
msgstr ""

#: ../mate-tweak:841
msgid "Metacity (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:843
msgid "Mutter (Elegant GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:845
msgid "Compiz (Advanced GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:862
msgid "Cupertino"
msgstr ""

#: ../mate-tweak:865
msgid "Fedora"
msgstr ""

#: ../mate-tweak:868
msgid "GNOME2"
msgstr ""

#: ../mate-tweak:871
msgid "Linux Mint"
msgstr ""

#: ../mate-tweak:874
msgid "Mageia"
msgstr ""

#: ../mate-tweak:879
msgid "Mutiny"
msgstr ""

#: ../mate-tweak:882
msgid "Netbook"
msgstr ""

#: ../mate-tweak:885
msgid "openSUSE"
msgstr ""

#: ../mate-tweak:888
msgid "Redmond"
msgstr ""

#: ../mate-tweak:891
msgid "Ubuntu MATE"
msgstr ""

#: ../mate-tweak:894
msgid "Wimpy"
msgstr ""

#: ../mate-tweak:944
msgid "Desktop"
msgstr "Benotzeriwerfläsch"

#: ../mate-tweak:945
msgid "Windows"
msgstr "Fënsteren"

#: ../mate-tweak:946
msgid "Interface"
msgstr "Interface"

#: ../mate-tweak:989
msgid "MATE Tweak"
msgstr ""

#: ../mate-tweak:992
msgid "Desktop icons"
msgstr "Benotzeriwerfläsch Symboler"

#: ../mate-tweak:993
msgid "Performance"
msgstr "Leschtung"

#: ../mate-tweak:994
msgid "Window Behaviour"
msgstr ""

#: ../mate-tweak:995
msgid "Appearance"
msgstr "Ausgesinn"

#: ../mate-tweak:996
msgid "Panels"
msgstr ""

#: ../mate-tweak:997
msgid "Panel Features"
msgstr ""

#: ../mate-tweak:998
msgid "Icons"
msgstr "Symboler"

#: ../mate-tweak:999
msgid "Context menus"
msgstr "Kontextmenu"

#: ../mate-tweak:1000
msgid "Toolbars"
msgstr "Toolbaren"

#: ../mate-tweak:1001
msgid "Window manager"
msgstr ""

#: ../mate-tweak:1003
#, fuzzy
msgid "Select the Desktop Icons you want enabled:"
msgstr "Wielt den Objekt deen dir op der Aarbeschtiwerfläsch gesin wellt"

#: ../mate-tweak:1004
#, fuzzy
msgid "Show Desktop Icons"
msgstr "Benotzeriwerfläsch Symboler"

#: ../mate-tweak:1005
msgid "Computer"
msgstr "Computer"

#: ../mate-tweak:1006
msgid "Home"
msgstr "Home"

#: ../mate-tweak:1007
msgid "Network"
msgstr "Netzwierk"

#: ../mate-tweak:1008
msgid "Trash"
msgstr "Poubell"

#: ../mate-tweak:1009
msgid "Mounted Volumes"
msgstr "Gemount Laafwierker"

#: ../mate-tweak:1011
msgid "Enable animations"
msgstr ""

#: ../mate-tweak:1012
msgid "Do not show window content when moving windows"
msgstr ""

#: ../mate-tweak:1013
msgid "Window manager performance tuning."
msgstr ""

#: ../mate-tweak:1015
msgid "Enable window snapping"
msgstr ""

#: ../mate-tweak:1016
msgid "Undecorate maximized windows"
msgstr ""

#: ../mate-tweak:1017
msgid "Do not auto-maximize new windows"
msgstr ""

#: ../mate-tweak:1019
msgid "Window control placement."
msgstr ""

#: ../mate-tweak:1021
msgid "Save Panel Layout"
msgstr ""

#: ../mate-tweak:1022
msgid "Enable indicators"
msgstr ""

#: ../mate-tweak:1023
msgid "Enable advanced menu"
msgstr ""

#: ../mate-tweak:1024
msgid "Enable keyboard LED"
msgstr ""

#: ../mate-tweak:1025
msgid "Enable launcher"
msgstr ""

#: ../mate-tweak:1026
msgid "Show icons on menus"
msgstr "Weist d'Iconer um Menu"

#: ../mate-tweak:1027
msgid "Show icons on buttons"
msgstr "Weist Iconer op den Knappchen"

#: ../mate-tweak:1028
msgid "Show Input Methods menu in context menus"
msgstr "Weist Eingabsmethodmenu am Kontextmenu"

#: ../mate-tweak:1029
msgid "Show Unicode Control Character menu in context menus"
msgstr "Weist Unicode Kontrolcharacktermenu am Kontextmenu"

#: ../mate-tweak:1031
msgid "Style:"
msgstr ""

#: ../mate-tweak:1032
msgid "Icon size:"
msgstr "Symbolgréisst:"

#: ../mate-tweak:1051
msgid "Small"
msgstr "Schmuel"

#: ../mate-tweak:1052
msgid "Large"
msgstr "Grouss"

#: ../mate-tweak:1058
msgid "Traditional (Right)"
msgstr ""

#: ../mate-tweak:1059
msgid "Contemporary (Left)"
msgstr ""

#: ../mate-tweak:1065
msgid "The new window manager will be activated upon selection."
msgstr ""

#: ../mate-tweak:1066
msgid "Select a window manager."
msgstr ""

#: ../mate-tweak:1072
msgid "Select a panel layout to change the user interface."
msgstr ""

#: ../mate-tweak:1073
msgid ""
"The new panel layout will be activated on selection and destroy any "
"customisations you might have made."
msgstr ""

#: ../mate-tweak:1086
msgid "Text below items"
msgstr "Text ennert dem Element"

#: ../mate-tweak:1087
msgid "Text beside items"
msgstr "Text nierft dem Element"

#: ../mate-tweak:1088
msgid "Icons only"
msgstr "Nemmen Iconer"

#: ../mate-tweak:1089
msgid "Text only"
msgstr "Nemmen Text"
